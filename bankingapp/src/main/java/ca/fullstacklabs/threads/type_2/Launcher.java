package ca.fullstacklabs.threads.type_2;

import ca.fullstacklabs.threads.type_1.ThreadLauncher;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static ca.fullstacklabs.threads.type_2.FileSplitter.*;

public class Launcher implements Runnable{
    static Map<String, Boolean> mapOfFiles = new HashMap<>();


    public static void main(String[] args) {
        Thread fSplitter = new Thread(new Launcher());
        fSplitter.start();
        mapOfFiles.put(file1,false);
        mapOfFiles.put(file2,false);
        mapOfFiles.put(file3,false);
        mapOfFiles.put(file4,false);
        Thread t1 = new Thread(new ThreadLauncher());
        t1.start();
        Thread t2 = new Thread(new ThreadLauncher());
        t2.start();
        Thread t3 = new Thread(new ThreadLauncher());
        t3.start();
        Thread t4 = new Thread(new ThreadLauncher());
        t4.start();
    }

    @Override
    public void run() {
        try {
            FileSplitter.doSplit();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlAccess.readDataBase(decideFilePath());

    }

    private String decideFilePath() {for (Map.Entry<String, Boolean> entry : mapOfFiles.entrySet()) {
        if (entry.getValue() == false) {
            mapOfFiles.remove(entry.getKey(), entry.getValue());
            System.out.println(Thread.currentThread().getName());
        }
    }
        return null;
    }
}
