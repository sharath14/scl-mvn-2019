package ca.fullstacklabs.file;

import java.sql.SQLException;

public interface Dao<T> {
    void readIntoDatabase(Transactions transactions) throws SQLException;
}
