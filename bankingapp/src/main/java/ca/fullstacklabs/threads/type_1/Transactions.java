package ca.fullstacklabs.threads.type_1;

import java.util.Date;

public class Transactions {
    String Account_No;
    Date TRANSACTION_DATE;
    String TRANSACTION_DETAILS;
    String CHQ_NO;
    Date VALUE_DATE;
    Double WITHDRAWAL_AMT ;
    Double DEPOSIT_AMT;
    Double BALANCE_AMT;

    public String getAccount_No() {
        return Account_No;
    }

    public void setAccount_No(String account_No) {
        Account_No = account_No;
    }

    public Date getTRANSACTION_DATE() {
        return TRANSACTION_DATE;
    }

    public void setTRANSACTION_DATE(Date TRANSACTION_DATE) {
        this.TRANSACTION_DATE = TRANSACTION_DATE;
    }

    public String getTRANSACTION_DETAILS() {
        return TRANSACTION_DETAILS;
    }

    public void setTRANSACTION_DETAILS(String TRANSACTION_DETAILS) {
        this.TRANSACTION_DETAILS = TRANSACTION_DETAILS;
    }

    public String getCHQ_NO() {
        return CHQ_NO;
    }

    public void setCHQ_NO(String CHQ_NO) {
        this.CHQ_NO = CHQ_NO;
    }

    public Date getVALUE_DATE() {
        return VALUE_DATE;
    }

    public void setVALUE_DATE(Date VALUE_DATE) {
        this.VALUE_DATE = VALUE_DATE;
    }

    public Double getWITHDRAWAL_AMT() {
        return WITHDRAWAL_AMT;
    }

    public void setWITHDRAWAL_AMT(Double WITHDRAWAL_AMT) {
        this.WITHDRAWAL_AMT = WITHDRAWAL_AMT;
    }

    public Double getDEPOSIT_AMT() {
        return DEPOSIT_AMT;
    }

    public void setDEPOSIT_AMT(Double DEPOSIT_AMT) {
        this.DEPOSIT_AMT = DEPOSIT_AMT;
    }

    public Double getBALANCE_AMT() {
        return BALANCE_AMT;
    }

    public void setBALANCE_AMT(Double BALANCE_AMT) {
        this.BALANCE_AMT = BALANCE_AMT;
    }

    @Override
    public String toString() {
        return "Transactions{" +
                "Account_No='" + Account_No + '\'' +
                ", TRANSACTION_DATE=" + TRANSACTION_DATE +
                ", TRANSACTION_DETAILS='" + TRANSACTION_DETAILS + '\'' +
                ", CHQ_NO='" + CHQ_NO + '\'' +
                ", VALUE_DATE=" + VALUE_DATE +
                ", WITHDRAWAL_AMT=" + WITHDRAWAL_AMT +
                ", DEPOSIT_AMT=" + DEPOSIT_AMT +
                ", BALANCE_AMT=" + BALANCE_AMT +
                '}';
    }
}