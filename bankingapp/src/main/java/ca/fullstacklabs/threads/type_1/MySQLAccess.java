package ca.fullstacklabs.threads.type_1;

import java.io.*;
import java.sql.*;
import java.text.ParseException;

public class MySQLAccess {


    public static synchronized void readDataBase(String filepath) {
        System.out.println(filepath + "current thread with filepath" + Thread.currentThread().getName());
        Transactions transactions = new Transactions();
        Connection connection = null;
        String[] columns;
        PreparedStatement preparedStatement = null;
        String line;
        String insertSqlQuery = "Insert into Transactions (Account_No,TRANSACTION_DATE,TRANSACTION_DETAILS,CHQ_NO,VALUE_DATE,WITHDRAWAL_AMT,DEPOSIT_AMT,BALANCE_AMT)values(?,?,?,?,?,?,?,?)";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        } catch (InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sharathDb", "root", "");
            if (connection != null) {
            }
        } catch (Exception e) {
        }
        try {
            preparedStatement = connection.prepareStatement(insertSqlQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            File f1 = new File(filepath);
            BufferedReader br1 = new BufferedReader(new FileReader(f1));
            br1.readLine();
            while ((line = br1.readLine()) != null) {
                columns = line.split(",");
                if (columns.length > 8) {
                    continue;
                }
                transactions.setAccount_No(columns[0]);
                transactions.setTRANSACTION_DATE(DataHelper.parseDate(columns[1]));
                transactions.setTRANSACTION_DETAILS(columns[2]);
                transactions.setCHQ_NO(columns[3]);
                transactions.setVALUE_DATE(DataHelper.parseDate(columns[4]));
                transactions.setWITHDRAWAL_AMT(DataHelper.StringToDouble(columns[5]));
                transactions.setDEPOSIT_AMT(DataHelper.StringToDouble(columns[6]));
                transactions.setBALANCE_AMT(DataHelper.StringToDouble(columns[7]));
                try {
                    preparedStatement.setString(1, transactions.getAccount_No());
                    preparedStatement.setDate(2, new java.sql.Date(transactions.getTRANSACTION_DATE().getTime()));
                    preparedStatement.setString(3, transactions.getTRANSACTION_DETAILS());
                    preparedStatement.setString(4, transactions.getCHQ_NO());
                    preparedStatement.setDate(5, new java.sql.Date(transactions.getVALUE_DATE().getTime()));
                    preparedStatement.setDouble(6, transactions.getWITHDRAWAL_AMT());
                    preparedStatement.setDouble(7, transactions.getDEPOSIT_AMT());
                    preparedStatement.setDouble(8, transactions.getBALANCE_AMT());
                    preparedStatement.executeUpdate();
                    //System.out.println("inserted transaction" + transactions);
                } catch (SQLException sqle) {
                    System.out.println("SQLException :" + sqle.getMessage());
                    sqle.printStackTrace();
                } finally {
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


