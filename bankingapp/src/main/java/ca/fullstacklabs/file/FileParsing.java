package ca.fullstacklabs.file;

import ca.fullstacklabs.threads.type_1.DataHelper;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;

public class FileParsing {
    public void doFileParsing() throws SQLException {
        String[] columns;
        String line;
        File file = new File("/Users/sharathchandra/Downloads/banktransactions.csv");
        BufferedReader brdr;
        try {
            brdr = new BufferedReader(new FileReader(file));
            brdr.readLine();
            while ((line = brdr.readLine()) != null) {
                columns = line.split(",");
                if (columns.length > 8) {
                    continue; }
                Transactions transactions = new Transactions();
                transactions.setAccount_No(columns[0]);
                transactions.setTRANSACTION_DATE(ca.fullstacklabs.threads.type_1.DataHelper.parseDate(columns[1]));
                transactions.setTRANSACTION_DETAILS(columns[2]);
                transactions.setCHQ_NO(columns[3]);
                transactions.setVALUE_DATE(ca.fullstacklabs.threads.type_1.DataHelper.parseDate(columns[4]));
                transactions.setWITHDRAWAL_AMT(ca.fullstacklabs.threads.type_1.DataHelper.StringToDouble(columns[5]));
                transactions.setDEPOSIT_AMT(ca.fullstacklabs.threads.type_1.DataHelper.StringToDouble(columns[6]));
                transactions.setBALANCE_AMT(DataHelper.StringToDouble(columns[7]));
                TransactionsDaoImpl txDao = new TransactionsDaoImpl();
                txDao.readIntoDatabase(transactions);
           }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException | ParseException e) {
            e.printStackTrace(); }
    }
}
