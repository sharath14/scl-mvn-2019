package ca.fullstacklabs.file;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    public static Connection getConnection() throws SQLException {
        Connection connection;

        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sharathDb", "root", "");
        return connection;
    }
}
