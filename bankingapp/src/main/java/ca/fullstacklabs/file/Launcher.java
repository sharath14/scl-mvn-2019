package ca.fullstacklabs.file;

public class Launcher {
    public static void main(String[] args) throws Exception {
        FileParsing fileParsing = new FileParsing();
        fileParsing.doFileParsing();
    }
}
