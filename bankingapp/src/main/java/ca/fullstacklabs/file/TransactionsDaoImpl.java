package ca.fullstacklabs.file;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TransactionsDaoImpl implements Dao<Transactions> {
    String insertSqlQuery = "Insert into Transactions (Account_No,TRANSACTION_DATE,TRANSACTION_DETAILS,CHQ_NO,VALUE_DATE,WITHDRAWAL_AMT,DEPOSIT_AMT,BALANCE_AMT)values(?,?,?,?,?,?,?,?)";
    Connection connection = ConnectionFactory.getConnection();
    PreparedStatement preparedStatement = connection.prepareStatement(insertSqlQuery);

    public TransactionsDaoImpl() throws SQLException {
    }

    @Override
    public void readIntoDatabase(Transactions transactions) throws SQLException {
        try {

            preparedStatement.setString(1, transactions.getAccount_No());
            preparedStatement.setDate(2, new java.sql.Date(transactions.getTRANSACTION_DATE().getTime()));
            preparedStatement.setString(3, transactions.getTRANSACTION_DETAILS());
            preparedStatement.setString(4, transactions.getCHQ_NO());
            preparedStatement.setDate(5, new java.sql.Date(transactions.getVALUE_DATE().getTime()));
            preparedStatement.setDouble(6, transactions.getWITHDRAWAL_AMT());
            preparedStatement.setDouble(7, transactions.getDEPOSIT_AMT());
            preparedStatement.setDouble(8, transactions.getBALANCE_AMT());
            preparedStatement.executeUpdate();
            System.out.println("inserted transaction" + transactions.toString());
        } catch (SQLException sqle) {
            System.out.println("SQLException :" + sqle.getMessage());
            sqle.printStackTrace(); }
        connection.close();

    }
}

